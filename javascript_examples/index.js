"use strict"; // use strict mode to prevent some pitfalls of javascript

let username; // js 6 var declaration
console.log(username); // this is  currently 'undefined'

username = "spencer";
console.log(username); // this is  now 'spencer'

console.log("You found me! Hello World!!");
let testing = "hello world :)"
console.log(testing);
console.log(window);

