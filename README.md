What's a README.md?
===================
A README file is documentation for the current repository, it uses markdown (.md) syntax.
It can describe what the project is for, usages/examples, troubleshooting steps, links to helpful sites, etc.
There are various ways to format a README file, see below.
There are LOADS of features withing GitLab for rendering markdown files.
For a full list, see [here][gitlab_markdown_link].

[gitlab_markdown_link]: https://docs.gitlab.com/ee/user/markdown.html


## Headers
```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternate H1
============

Alternate H2
------------
```
# H1
## H2
### H3
#### H4
##### H5
###### H6
Alternate H1
============
Alternate H2
------------
<br>
<br>

## Formatting Lines
```
Hyphens
---

Asterisks
***

Underscores
___
```
Hyphens
---

Asterisks
***

Underscores
___
<br>
<br>

## Styling
```
*italics* or _italics_
**bold** or __bold__
**bold and _italics_**
~~strikethrough~~
```

*italics* or _italics_
**bold** or __bold__
**bold and _italics_**
~~strikethrough~~
<br>
<br>

## Lists
```
1. Item 1
2. Item 2
    - Sub-Item 2a
    - Sub-Item 2b
3. Item 3
    1. Sub-Item 3.1
    2. Sub-Item 3.2
4. Item 4
    - [x] Completed Sub-Task
    - [ ] Incomplete Sub-Task
```
1. Item 1
2. Item 2
    - Sub-Item 2a
    - Sub-Item 2b
3. Item 3
    1. Sub-Item 3.1
    2. Sub-Item 3.2
4. Item 4
    - [x] Completed Sub-Task
    - [ ] Incomplete Sub-Task
<br>
<br>

## Links
```
[inline link](https://example.org)
[inline link - hover over me](https://example.org "This can be titled anything!")
[text reference link][case-insensitive reference text]
[num reference link][1]

[cAse-iNSeNsiTiVE reference TEXT]: https://example.org
[1]: https://google.com
```

[inline link](https://example.org)
<br>
[inline link w/ title - hover over me](https://example.org "This can be titled anything!")
<br>
[text reference link][case-insensitive reference text]
<br>
[num reference link][1]

[cAse-iNSeNsiTiVE reference TEXT]: https://example.org
[1]: https://google.com
<br>
<br>

## Code Snippets
```
Inline `code` uses back-ticks.
```
Inline `code` uses back-ticks.

Blocks of code can be represented by three back-ticks. You can also take advantage
of syntax highlighting.

~~~
```javascript
let myVar = "What's your name?";
prompt(myVar);
```
~~~

```javascript
let myVar = "What's your name?";
prompt(myVar);
```
<br>
<br>

## Tables
Tables are useful for various cases.
```
| Left-Algined | Centered | Right-Aligned |
| ------------ | :------: | ------------: |
| left         | center   | right         |
| **bold**     | `code`   | *italics*     |
| 123          | 456      | 789           |
```

| Left-Algined | Centered | Right-Aligned |
| ------------ | :------: | ------------: |
| left         | center   | right         |
| **bold**     | `code`   | *italics*     |
| 123          | 456      | 789           |
<br>
<br>

## Quotes
Quoting or formatting a reply-like style can be helpful for getting a point across.
```
How do you quote someone?
> Like this! **bold** *italics* ~~strikethrough~~ `code`
```

How do you quote someone?
> Like this! **bold** *italics* ~~strikethrough~~ `code`

